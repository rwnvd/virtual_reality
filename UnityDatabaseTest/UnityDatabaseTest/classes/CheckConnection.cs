﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using UnityDatabaseTest.classes;
using System.Windows.Forms;

namespace UnityDatabaseTest.classes
{
    class CheckConnection
    {
        public bool HasConnection()
        {
            try
            {
                if (Config.conn.State == System.Data.ConnectionState.Closed)
                {
                    Config.conn.Open();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return false;
            }
            finally
            {
                Config.conn.Close();
            }
        }
    }
}
