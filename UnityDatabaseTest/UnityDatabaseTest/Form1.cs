﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UnityDatabaseTest.classes;

namespace UnityDatabaseTest
{
    public partial class Form1 : Form
    {

        private Int32 sumID;
        private string answer;
        private Int32 playerID;
        private Int32 gegevenantwoord;

        public Form1()
        {
            InitializeComponent();
            getSum();
        }

        public void getSum()
        {
            var instance = new CheckConnection();
            if (instance.HasConnection())
            {
                try
                {
                    if (Config.conn.State == System.Data.ConnectionState.Closed)
                        Config.conn.Open();
                    MySqlCommand sqlCmd = new MySqlCommand(Querys.getSumQuery, Config.conn);
                    MySqlDataReader dr; // Nieuwe data reader aanmaken

                    dr = sqlCmd.ExecuteReader(); // Voer reader uit
                    while (dr.Read())
                    {

                        sumID = dr.GetInt32("somID");
                        answer = dr.GetString("antwoord");
                        lblSom.Text = dr.GetString("som");
                    }
                    dr.Close();
                }
                catch (Exception ex) // Alle exceptions worden hier in opgevangen ( Deze worden al van zichzelf duidelijk omschreven ) .
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    if (Config.conn.State == System.Data.ConnectionState.Open) // En nu dat alles klaar is, sluiten we de verbinding met de Database.
                    {
                        Config.conn.Close();
                    }

                }
            }
            else
            {
                MessageBox.Show("Er kon geen verbinding gemaakt worden met onze database!");
                txtBoxAnswer.Enabled = false;
                lblSom.Enabled = false;
                txtBoxPlayerName.Enabled = false;
                btnCheckAnswer.Enabled = false;
                btnRefresh.Enabled = false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            getSum();
        }

        private void btnCheckAnswer_Click(object sender, EventArgs e)
        {
            gegevenantwoord = Convert.ToInt32(txtBoxAnswer.Text);
            playerID = Convert.ToInt32(txtBoxPlayerName.Text);
            if (txtBoxAnswer.Text == answer && txtBoxPlayerName.Text != "")
            {
                gegevenantwoord = Convert.ToInt32(txtBoxAnswer.Text);
                MessageBox.Show("Correct!");
                // Schrijf weg naar de database
                try
                {
                    if(Config.conn.State == ConnectionState.Closed)
                    {
                        Config.conn.Open();
                    }
                    MySqlCommand sqlCmd = new MySqlCommand(Querys.insertScoreQuery(playerID, Config.scorePoints, sumID, gegevenantwoord), Config.conn);
                    sqlCmd.ExecuteNonQuery(); // Voer QUERY uit
                    Config.conn.Close();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(Convert.ToString(ex));
                }

                getSum();
            }
            else
            {
                MessageBox.Show("Dit is helaas niet het juiste antwoord!");
                try
                {
                    if (Config.conn.State == ConnectionState.Closed)
                    {
                        Config.conn.Open();
                    }
                    MySqlCommand sqlCmd = new MySqlCommand(Querys.insertScoreQuery(playerID, 0, sumID, gegevenantwoord), Config.conn);
                    sqlCmd.ExecuteNonQuery(); // Voer QUERY uit
                    Config.conn.Close();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(Convert.ToString(ex));
                }
                getSum();
            }
        }
    }
}
