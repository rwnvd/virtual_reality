﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllText : MonoBehaviour {

    public TextMesh sommen;
    public Text name;
    public Text count;
    public Text score;
    public Text somAantal;
    public Text id;

    public void SetTextSom(string var)
    {
        sommen.text = var;
    }

    public void SetTextCount(string var)
    {
        count.text = var;
    }

    public void SetTextScore(string var)
    {
        score.text = var;
    }

    internal void SetTextId(string text)
    {
        id.text = text;
    }

    public void SetTextName(string var)
    {
        name.text = var;
    }

    public void SetTextSommenAantal(string var)
    {
        somAantal.text = var;
    }
}
