﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    private int score;
    public Transform myObject;

    // Use this for initialization
    void Start () {
        score = 0;
	}
	
	public void displayScore()
    {
        myObject.GetComponent<AllText>().SetTextScore(score+"");
    }

    public void setScore(int i)
    {
        score = score + i;
    }
}
