﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomAantal : MonoBehaviour {

    private int som;
    public Transform myObject;

    // Use this for initialization
    void Start () {
        som = 0;
	}
	
	public void displaySomAantal()
    {
        myObject.GetComponent<AllText>().SetTextSommenAantal(som + "/ 10");
    }

    public void setSomAantal()
    {
        som = som + 1;
    }
}
