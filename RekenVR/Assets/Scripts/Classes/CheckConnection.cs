﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckConnection : MonoBehaviour {

  
        public bool HasConnection()
        {
            try
            {
                if (Config.conn.State == System.Data.ConnectionState.Closed)
                {
                    Config.conn.Open();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
                return false;
            }
            finally
            {
                Config.conn.Close();
            }
        }
   
}
