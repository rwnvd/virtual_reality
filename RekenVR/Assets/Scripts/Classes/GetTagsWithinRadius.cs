﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTagsWithinRadius : MonoBehaviour {

    private List<int> numbers = new List<int>();
    public Transform game;
    private Vector3 scale = new Vector3(20,20,20);
    public Transform myObject;
    

    //debug
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(game.position, scale);
    }

    //all is the total values of all the blocks within the fences
    public int GetTags()
    {
        int all = 0;
        Collider[] tags = Physics.OverlapBox(game.position, scale / 2); //scale of overlapBox = transformscale /2
        foreach (Collider ob in tags)
        {
            if (ob.gameObject.tag == "Cube")
            {
                numbers.Add(int.Parse(ob.gameObject.transform.Find("var").tag));
            }
        }
        foreach (int var in numbers)
        {
            all = all + var;
        }
        debug(all);
        int all2 = all;
        all = 0;
        return all2;
    }
    

    void debug(int all)
    {
        foreach (int var in numbers)
        {
            Debug.Log(":::::::::DEGUBLOG" + var +"");
        }
        Debug.Log(":::::::::DEGUBLOG" + all + "");
        Debug.Log("---------------------------------------");
    }
}
