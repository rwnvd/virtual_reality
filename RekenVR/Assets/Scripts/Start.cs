﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Start : MonoBehaviour {

    public Transform myObject;
    public Transform parent;

	public void CollectName()
    {
        Debug.Log(transform.GetComponentInChildren<Text>().text);
        myObject.GetComponent<AllText>().SetTextName(transform.Find("Text").GetComponent<Text>().text);
        myObject.GetComponent<AllText>().SetTextId(transform.Find("Id").GetComponent<Text>().text);
        foreach (Transform child in parent) //parent == NameList
        {
            GameObject.Destroy(child.gameObject);
        }
        myObject.GetComponent<Score>().displayScore();
    }
}
