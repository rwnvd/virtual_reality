﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MySql.Data.MySqlClient;

public class SomGenerator : MonoBehaviour {

    private Int32 sumID;
    private string answer;
    private Int32 playerID;
    private Int32 gegevenantwoord;
    public Transform myObject;

    public void getSum()
    {
        var instance = new CheckConnection();
        if (instance.HasConnection())
        {
            try
            {
                if (Config.conn.State == System.Data.ConnectionState.Closed)
                    Config.conn.Open();
                MySqlCommand sqlCmd = new MySqlCommand(Querys.getSumQuery, Config.conn);
                MySqlDataReader dr; // Nieuwe data reader aanmaken

                dr = sqlCmd.ExecuteReader(); // Voer reader uit
                while (dr.Read())
                {

                    sumID = dr.GetInt32("somID");
                    answer = dr.GetString("antwoord");
                    //Display som here!!!!!!!!!!!!!!!!
                    myObject.GetComponent<AllText>().SetTextSom(dr.GetString("som"));
                }
                dr.Close();
            }
            catch (Exception ex) // Alle exceptions worden hier in opgevangen ( Deze worden al van zichzelf duidelijk omschreven ) .
            {
                Debug.Log(ex.Message);
            }
            finally
            {
                if (Config.conn.State == System.Data.ConnectionState.Open) // En nu dat alles klaar is, sluiten we de verbinding met de Database.
                {
                    Config.conn.Close();
                }

            }
        }
        else
        {
            Debug.Log("Er kon geen verbinding gemaakt worden met onze database!");
        }
    }

    public bool checkAwnser(string awnser, string name)
    {
        
        gegevenantwoord = Convert.ToInt32(awnser);
        playerID = Convert.ToInt32(name);
        //Lisa's add voor list met namen in db dan moet playerID weg
        //MySqlDataReader dr2; // Nieuwe data reader aanmaken
        //MySqlCommand sqlCmd2 = new MySqlCommand(Querys.getIDByName(name), Config.conn);
        //dr2 = sqlCmd2.ExecuteReader(); // Voer reader uit
        //playerID = dr2.GetInt32("deelnemerID");
        if (awnser == answer && name != "")
        {
            gegevenantwoord = Convert.ToInt32(awnser);
            // Schrijf weg naar de database
            mysqlCommand(Config.scorePoints);
            myObject.GetComponent<Score>().setScore(Config.scorePoints);
            return true;
        }
        else
        {
            mysqlCommand(0);
            myObject.GetComponent<Score>().setScore(0);
            return false;
        }

    }

    private void mysqlCommand(Int32 count)
    {
        try
        {
            if (Config.conn.State == System.Data.ConnectionState.Closed)
            {
                Config.conn.Open();
            }
            MySqlCommand sqlCmd = new MySqlCommand(Querys.insertScoreQuery(playerID, count, sumID, gegevenantwoord), Config.conn);
            sqlCmd.ExecuteNonQuery(); // Voer QUERY uit
            Config.conn.Close();
        }
        catch (Exception ex)
        {

            Debug.Log(Convert.ToString(ex));
        }
        myObject.GetComponent<Score>().setScore(count);
    }
}
