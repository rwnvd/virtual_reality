﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sommen : MonoBehaviour {

    public TextMesh text;
    public Text count;
    private List<string> sommen = new List<string>();
    public int aantal;
    public int rangeNumberVanaf;
    public int rangeNumberTot;

    // Use this for initialization
    void Start () {
        ConsoleText();
    }
	
	// Update is called once per frame
	void Update () {
	}

    private void ConsoleText()
    {
        if (sommen.Count <= aantal)
        {
            //count.text = "(" + sommen.Count + "/ " + aantal + ")";
            string som = GetSom(rangeNumberVanaf, rangeNumberTot);
            text.text= som;
        }
        else
        {
            text.text = "Afgelopen";
        }
    }

    private string Controleer(string som, string userAwnser)
    {
        int awnser = GetAwnser(som);
        if (awnser == Int32.Parse(userAwnser))
        {
            return "goed";
        }
        return "fout";
    }

    private string GetSom(int radiusOne, int radiusTwo)
    {
        System.Random rnd = new System.Random();
        int firstNumber = rnd.Next(radiusOne, radiusTwo);
        int secondNumber = rnd.Next(radiusOne, radiusTwo);
        List<string> list = new List<string>
            {
                "x",
                ":",
                "+",
                "-"
            };

        int r = rnd.Next(list.Count);
        string teken = (string)list[r];
        string som = firstNumber + " " + teken + " " + secondNumber;
        if (GetAwnser(som) <= 0)
        {
            foreach (string vorigeSommen in sommen)
            {
                if (som == vorigeSommen)
                {
                    som = GetSom(radiusOne, radiusTwo);
                }
            }
        }
        return som;
    }

    //for awnser input: example "9 + 0" with whitespace inbetween
    private int GetAwnser(string som)
    {
        int awnser = 0;
        string[] splitSom = som.Split(null);
        switch (splitSom[1])
        {
            case "x":
                awnser = Int32.Parse(splitSom[0]) * Int32.Parse(splitSom[2]);
                break;
            case ":":
                awnser = Int32.Parse(splitSom[0]) / Int32.Parse(splitSom[2]);
                break;
            case "+":
                awnser = Int32.Parse(splitSom[0]) + Int32.Parse(splitSom[2]);
                break;
            case "-":
                awnser = Int32.Parse(splitSom[0]) - Int32.Parse(splitSom[2]);
                break;
        }
        return awnser;
    }
}
