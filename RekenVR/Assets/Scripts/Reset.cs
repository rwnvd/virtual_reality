﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour {

    public Transform one;
    public Transform five;
    public Transform ten;
    public Transform hundred;
    private Vector3 oneVector = new Vector3();
    private Vector3 fiveVector = new Vector3();
    private Vector3 tenVector = new Vector3();
    private Vector3 hundredVector = new Vector3();
    private Quaternion oneRotation = new Quaternion();
    private Quaternion fiveRotation = new Quaternion();
    private Quaternion tenRotation = new Quaternion();
    private Quaternion hundredRotation = new Quaternion();
    public Transform parent;
    public Transform myObject;
    

	// Use this for initialization
	void Start () {
        oneVector = one.position;
        oneRotation = one.rotation;
        fiveVector = five.position;
        fiveRotation = five.rotation;
        tenVector = ten.position;
        tenRotation = ten.rotation;
        hundredVector = hundred.position;
        hundredRotation = hundred.rotation;
        
	}
	
	public void ResetScene()
    {
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Cube"))
        {
            if (fooObj.name == "clone")
            {
                GameObject.Destroy(fooObj.gameObject);
            }
        }
        one.position = oneVector;
        one.rotation = oneRotation;
        five.position = fiveVector;
        five.rotation = fiveRotation;
        ten.position = tenVector;
        ten.rotation = tenRotation;
        hundred.position = hundredVector;
        hundred.rotation = hundredRotation;

        //one.GetComponent<Clone>().seti();
        //five.GetComponent<Clone>().seti();
        //ten.GetComponent<Clone>().seti();
        //hundred.GetComponent<Clone>().seti();

        myObject.GetComponent<Score>().displayScore();
        myObject.GetComponent<SomAantal>().displaySomAantal();
    }
}
