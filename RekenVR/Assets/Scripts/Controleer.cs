﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controleer : MonoBehaviour {

    public Transform myObject;
    public Transform name;
    private int count;

    private void Start()
    {
        count = 0;
    }

    public void controleer()
    {
        int all = myObject.GetComponent<GetTagsWithinRadius>().GetTags();
        
        bool goodfalse = myObject.GetComponent<SomGenerator>().checkAwnser(all + "", name.GetComponent<Text>().text);
        Debug.Log("bool = " + goodfalse);
        if (goodfalse == true)
        {
            Debug.Log("Goed antwoord!");
        }
        else
        {
            Debug.Log("Fout antwoord!");
        }
        if (count < 10)
        {
            myObject.GetComponent<SomGenerator>().getSum();
            count = count+1;
            myObject.GetComponent<SomAantal>().setSomAantal();
        }
        else
        {
            myObject.GetComponent<AllText>().SetTextSom("Afgelopen!");
        }
    }
}
