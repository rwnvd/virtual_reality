﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clone : MonoBehaviour {

    public Transform parent;
    private Vector3 startPosition;
    bool i;

    void Start()
    {
        i = true;
        startPosition = transform.position;
    }


    // Update is called once per frame
    void Update() {
        Vector3 finalPos = transform.position;
        if (startPosition.x != finalPos.x || startPosition.z != finalPos.z) //No y because it has a rigidbody
        {
            if (i)
            {
                var prefab = Instantiate(transform, startPosition, Quaternion.identity);
                transform.name = "clone";
                prefab.transform.localScale = new Vector3(0.17403F, 0.17403F, 0.17403F);
                prefab.transform.rotation = Quaternion.Euler(0, -90, 0);
                i = false;
            }
        }
    }

    public void seti()
    {
        i = true;
    }
}


