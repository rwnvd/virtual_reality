﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListWithNames : MonoBehaviour {

    public Button buttonPrefab;
    public Image loadCanvas;
    public List<string> names = new List<string>();
    private int id = 1;

	// Use this for initialization
	void Start () {
        loadList();
        foreach (string name in names) {
            //Cast it as a Button, not a game object
            Button newButton = (Button)Instantiate(buttonPrefab);
            //Use .SetParent(canvasName,false)    
            newButton.transform.SetParent(loadCanvas.transform, false);
            newButton.GetComponentInChildren<Text>().text = name;
            //get id
            newButton.transform.Find("Id").GetComponent<Text>().text = id + "";
            id++;
        }
        Destroy(buttonPrefab.gameObject);
    }

    void loadList()
    {
        names.Add("Lotte");
        names.Add("Kim");
        names.Add("Robin");
        names.Add("Chen");
        names.Add("Koen");
        names.Add("Marie");
        names.Add("Oliver");
        names.Add("William");
        names.Add("James");

        //Hier moet Rowens code van de lijstjes komen
    }
}
